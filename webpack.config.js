const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'pages';
const mode = isProduction ? 'production' : 'development';

module.exports = {
  mode,
  entry: {
    script: './src/pages/index/script.js',
    main: './src/pages/main/main.js',
    about: './src/pages/about/about.js',
    methods: './src/pages/methods/methods.js',
    urology: './src/pages/urology/urology.js',
    patients: './src/pages/patients/patients.js',
    contacts: './src/pages/contacts/contacts.js',
    diagnostic: './src/pages/diagnostic/diagnostic.js',
    doctors: './src/pages/doctors/doctors.js',
    doctor: './src/pages/doctor/doctor.js',
    vacancy: './src/pages/vacancy/vacancy.js',
    prices: './src/pages/prices/prices.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].js',
  },
  devtool: isProduction ? undefined : 'source-map',
  plugins: [new ESLintPlugin()],
  optimization: {
    minimize: isProduction,
  },
};
