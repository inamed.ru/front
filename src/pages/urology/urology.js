/* eslint-disable no-new */
import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';
import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { ArticleItem } from '../../blocks/article-item/article-item';
import { DoctorItemSlider } from '../../blocks/doctor-item-slider/doctor-item-slider';
import { toggleReviewText, ReviewsSlider } from '../../blocks/reviews/reviews';

document.addEventListener('DOMContentLoaded', () => {
  new HomeTreatment();
  accordionItem();
  new DoctorItemSlider();
  new ArticleItem();
  new ReviewsSlider();
  toggleReviewText();
});
