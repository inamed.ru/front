import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { toggleReviewText, ReviewsSlider } from '../../blocks/reviews/reviews';
import { iframeLoading } from '../../blocks/iframe-responsive/iframe-responsive';
import { doctorInfoInit } from '../../blocks/doctor-info/doctor-info';

document.addEventListener('DOMContentLoaded', () => {
  accordionItem();
  toggleReviewText();
  doctorInfoInit();
  iframeLoading();
  // eslint-disable-next-line no-new
  new ReviewsSlider();
});
