import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';
import { homeContacts } from '../../blocks/home-contacts/home-contacts';
import { homeGalleryInit } from '../../blocks/home-gallery/home-gallery';
import counters from '../../common/js/modules/counters';

document.addEventListener('DOMContentLoaded', () => {
  const countersCodeBody = `
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
  `;
  counters(countersCodeBody);
  homeGalleryInit();
  accordionItem();
  // eslint-disable-next-line no-new
  new HomeTreatment();
  homeContacts.init();
});
