/* eslint-disable no-new */
import Slider from '../../blocks/slider/slider';
import footerUpdateYear from '../../blocks/footer/footer';
import submitForm from '../../common/js/modules/submit-form';
import { phoneMask } from '../../blocks/form/form';
import lb from '../../common/js/modules/lightbox';
import counters from '../../common/js/modules/counters';
import { Header } from '../../blocks/header/header';
import media from '../../common/js/modules/match-media';
import Select from '../../blocks/choices/сhoices';

window.Slider = Slider;

document.addEventListener('DOMContentLoaded', () => {
  window.media = media;
  media.init();
  const countersMetrika = `
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
      (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();
      for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
      k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

      ym(97974098, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
      });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/97974098" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
  `;
  counters(countersMetrika, 'head');
  footerUpdateYear();
  submitForm();
  phoneMask.init();
  lb();

  new Header(document.querySelector('.header'));
  const selects = document.querySelectorAll('select');
  selects.forEach((select) => {
    const selectItem = new Select(select, {
      searchEnabled: false, // отключить поиск
      itemSelectText: '', // текст, который отображается для каждого выбранного элемента в множественном списке
      shouldSort: false, // отключить сортировку по алфавиту
    });
    selectItem.init();
  });
});
