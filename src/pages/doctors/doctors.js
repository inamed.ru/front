import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';
import { homeGalleryInit } from '../../blocks/home-gallery/home-gallery';
import { accordionItem } from '../../blocks/accordion-item/accordion-item';

document.addEventListener('DOMContentLoaded', () => {
  homeGalleryInit();
  // eslint-disable-next-line no-new
  new HomeTreatment();
  accordionItem();
});
