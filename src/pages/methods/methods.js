/* eslint-disable no-new */
import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';
import { DoctorItemSlider } from '../../blocks/doctor-item-slider/doctor-item-slider';
import { toggleReviewText, ReviewsSlider } from '../../blocks/reviews/reviews';

document.addEventListener('DOMContentLoaded', () => {
  accordionItem();
  new HomeTreatment();
  new DoctorItemSlider();
  new ReviewsSlider();
  toggleReviewText();
});
