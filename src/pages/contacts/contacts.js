import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';
import { homeGalleryInit } from '../../blocks/home-gallery/home-gallery';
import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { HomeContacts } from '../../blocks/home-contacts/home-contacts';

document.addEventListener('DOMContentLoaded', () => {
  homeGalleryInit();
  // eslint-disable-next-line no-new
  new HomeTreatment();
  // eslint-disable-next-line no-new
  new HomeContacts(document.querySelector('.contacts-map'));
  accordionItem();
  const licensesGallery = document.querySelector('.licenses-gallery .swiper');
  const licensesGalleryInit = () => {
    if (licensesGallery) {
      const licensesGallerySlider = new window.Slider(licensesGallery, {
        spaceBetween: 32,
        breakpoints: {
          320: {
            slidesPerView: 1,
          },
          480: {
            slidesPerView: 3,
          },
          800: {
            slidesPerView: 4,
          },
        },
      });
      licensesGallerySlider.init();
    }
  };
  licensesGalleryInit();
});
