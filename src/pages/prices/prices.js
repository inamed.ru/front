/* eslint-disable no-new */
import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { toggleReviewText, ReviewsSlider } from '../../blocks/reviews/reviews';
import { PriceList } from '../../blocks/price-list/price-list';

document.addEventListener('DOMContentLoaded', () => {
  accordionItem();
  toggleReviewText();
  new ReviewsSlider();
  new PriceList();
});
