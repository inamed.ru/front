import modal from '../../blocks/modal/modal';
import { homeContacts } from '../../blocks/home-contacts/home-contacts';
import counters from '../../common/js/modules/counters';

document.addEventListener('DOMContentLoaded', () => {
  const countersCodeBody = `
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
  `;
  counters(countersCodeBody);
  homeContacts.init();
  modal();
});
