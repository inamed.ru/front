import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';

document.addEventListener('DOMContentLoaded', () => {
  accordionItem();
  // eslint-disable-next-line no-new
  new HomeTreatment();
});
