const footerUpdateYear = () => {
  const footerElement = document.querySelector('.footer__year');
  const currentYear = new Date().getFullYear();
  let footerText = footerElement.innerHTML;
  footerText = footerText.replace(/\d{4}(?=\D*$)/, currentYear);
  footerElement.innerHTML = footerText;
};

export default footerUpdateYear;
