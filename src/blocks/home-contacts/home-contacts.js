/* eslint-disable no-undef */
import media from '../../common/js/modules/match-media';

const initMap = () => {
  const map = document.getElementById('home-contacts-map');
  if (!map) return;

  const myMap = new ymaps.Map(map, {
    center: [55.787903, 37.61],
    controls: ['zoomControl'],
    zoom: 15,
  }, {
    searchControlProvider: 'yandex#search',
    autoFitToViewport: 'always',
  });
  // Создание точки с дефолтной иконкой
  const myPlacemark = new ymaps.Placemark([55.787903, 37.61], {
    balloonContent: 'ул. Образцова, д. 14',
  }, {
    preset: 'islands#icon', // Стиль иконки
    iconColor: '#2c66bc', // Цвет иконки
  });

  // Добавление точки на карту
  myMap.geoObjects.add(myPlacemark);

  const matchMediaMap = () => {
    media.addChangeListener('max', 'm', matchMediaMap);
    if (media.max('m')) myMap.setCenter([55.787903, 37.61]);
    else myMap.setCenter([55.787903, 37.60]);
  };

  matchMediaMap();
};

export class HomeContacts {
  #element;
  #isYandexMapShowed;

  constructor(element) {
    this.#element = element;
  }

  #scrollHandler = () => {
    if (!this.#isYandexMapShowed) {
      // eslint-disable-next-line no-undef
      ymaps.ready(initMap);
      this.#isYandexMapShowed = true;
      window.removeEventListener('scroll', this.#scrollHandler);
    }
  };

  init = () => window.addEventListener('scroll', this.#scrollHandler);
}

export const homeContacts = new HomeContacts(document.querySelector('.home-contacts'));
