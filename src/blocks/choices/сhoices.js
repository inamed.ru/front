/* eslint-disable no-new */
import Choices from 'choices.js';

class Select {
  #element;

  constructor(element, settings = {}) {
    this.#element = element;
    this.settings = settings;
  }

  init() {
    if (!this.#element) return;
    new Choices(this.#element, this.settings);
  }
}

export default Select;
