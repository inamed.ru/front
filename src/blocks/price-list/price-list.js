/* eslint-disable no-param-reassign */
export class PriceList {
  #element;
  #input;
  #tableNames;
  #tableRows;
  #tableHeaders;
  #tabContainer;
  #tabs;
  #tabActiveClass = 'price-list__tab--active';
  #tables;

  constructor() {
    this.#element = document.querySelector('.price-list');
    this.#input = this.#element?.querySelector('.price-list__search-input');
    this.#tables = this.#element?.querySelectorAll('.price-list__table');
    this.#tableNames = this.#element?.querySelectorAll('.price-table__name');
    this.#tableRows = this.#element?.querySelectorAll('.price-table__row');
    this.#tableHeaders = this.#element?.querySelectorAll('.price-table__header');
    this.#tabContainer = this.#element?.querySelector('.price-list__tabs');
    this.#tabs = this.#element?.querySelectorAll('.price-list__tab');
    this.init();
  }

  static #hideExtraRows(table) {
    const rows = table.querySelectorAll('.price-table__row');
    if (rows.length > 3) {
      rows.forEach((row, index) => {
        if (index >= 3) row.classList.add('price-table__row--hidden');
      });

      if (!table.querySelector('.price-table__show-more')) {
        const showMoreButton = document.createElement('a');
        showMoreButton.setAttribute('href', '#');
        showMoreButton.classList.add('price-table__show-more');
        showMoreButton.innerHTML = 'Показать ещё';
        table.appendChild(showMoreButton);
      }
    }
  }

  static #toggleRowVisibility(button, table) {
    const rows = table.querySelectorAll('.price-table__row');
    const hiddenRows = table.querySelectorAll('.price-table__row--hidden');

    if (hiddenRows.length > 0) {
      hiddenRows.forEach((row) => row.classList.remove('price-table__row--hidden'));
      button.innerHTML = 'Скрыть';
    } else {
      rows.forEach((row, index) => {
        if (index >= 3) row.classList.add('price-table__row--hidden');
      });
      button.innerHTML = 'Показать ещё';
    }
  }

  #showHideRows() {
    this.#tables.forEach((table) => PriceList.#hideExtraRows(table));
    this.#element.addEventListener('click', (evt) => {
      if (evt.target.classList.contains('price-table__show-more')) {
        evt.preventDefault();
        const button = evt.target;
        const table = button.closest('.price-table');
        PriceList.#toggleRowVisibility(button, table);
      }
    });
  }

  #onInputChange() {
    const inputValue = this.#input.value.toLowerCase();

    this.#tableRows.forEach((row, index) => {
      const nameElement = this.#tableNames[index];
      if (!nameElement.dataset.original) {
        nameElement.dataset.original = nameElement.innerHTML;
      }

      const originalText = nameElement.dataset.original;
      const lowerCaseOriginalText = originalText.toLowerCase();

      if (inputValue && lowerCaseOriginalText.includes(inputValue)) {
        const regex = new RegExp(`(${inputValue})`, 'gi');
        nameElement.innerHTML = originalText.replace(regex, '<mark>$1</mark>');
        row.style.display = '';
        row.classList.remove('price-table__row--hidden');
      } else {
        nameElement.innerHTML = originalText;
        row.style.display = inputValue ? 'none' : '';
      }
    });

    this.#tableHeaders.forEach((header) => {
      const table = header.closest('.price-table');
      const hasVisibleRows = Array.from(table.querySelectorAll('.price-table__row')).some((row) => row.style.display !== 'none');
      header.style.display = hasVisibleRows ? '' : 'none';
    });

    if (!inputValue) this.#tables.forEach((table) => PriceList.#hideExtraRows(table));
    else {
      this.#tables.forEach((table) => {
        const showMoreButton = table.querySelector('.price-table__show-more');
        if (showMoreButton) showMoreButton.remove();
      });
    }
  }

  #tabsInit() {
    this.#tabContainer.addEventListener('click', (evt) => {
      this.#tabs.forEach((tab) => tab.classList.remove(this.#tabActiveClass));
      evt.target.classList.add(this.#tabActiveClass);

      this.#tables.forEach((table) => {
        if (evt.target.dataset.tabTitle !== table.dataset.tab && evt.target.dataset.tabTitle !== 'all') {
          table.style.display = 'none';
        } else {
          table.style.display = '';
        }
      });
    });
  }

  init() {
    this.#input.addEventListener('input', this.#onInputChange.bind(this));
    this.#tabsInit();
    this.#showHideRows();
  }
}
