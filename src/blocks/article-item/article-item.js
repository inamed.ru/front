import { breakpoints } from '../../common/js/enums';

export class ArticleItem {
  #element = document.querySelector('.article-item').closest('.swiper');
  #slider;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 32,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 2,
      },
      [breakpoints.m]: {
        slidesPerView: 3,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      this.#slider.init();
    }
  };
}
