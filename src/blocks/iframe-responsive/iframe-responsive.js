/* eslint-disable no-param-reassign */
const iframes = document.querySelectorAll('.iframe-responsive__iframe iframe');

export const iframeLoading = () => {
  iframes.forEach((frame) => {
    if (!frame.dataset.src) return;
    const userEvents = () => {
      window.removeEventListener('scroll', userEvents);
      window.removeEventListener('mousemove', userEvents);
      frame.src = `${frame.dataset.src}`;
    };
    window.addEventListener('scroll', userEvents);
    window.addEventListener('mousemove', userEvents);
  });
};
