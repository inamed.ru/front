const homeGallery = document.querySelector('.home-gallery .swiper');
export const homeGalleryInit = () => {
  if (homeGallery) {
    const homeGallerySlider = new window.Slider(homeGallery, {
      spaceBetween: 32,
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        480: {
          slidesPerView: 2,
        },
        800: {
          slidesPerView: 3,
        },
      },
    });
    homeGallerySlider.init();
  }
};
