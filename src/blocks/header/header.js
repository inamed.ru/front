export class Header {
  #media = window.matchMedia('(max-width: 800px)');
  #element;
  #menuLinks;
  #lastKnownScrollPosition = 0;
  #viewportHeight = window.innerHeight;
  #priceListSearch;
  #isPriceListVisible = false;
  #priceListObserver;
  #switcher;
  #activeClass = 'header--active';

  constructor(element) {
    this.#element = element;
    this.#switcher = this.#element?.querySelector('.js-header-switcher');
    this.#priceListSearch = document.querySelector('.price-list__search');
    this.#menuLinks = this.#element?.querySelectorAll('.header-navigation__item a');
    this.#priceListObserver = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          this.#isPriceListVisible = true;
          this.#element.classList.add('header--fixed-down');
        } else {
          this.#isPriceListVisible = false;
          this.#element.classList.remove('header--fixed-down');
        }
      });
    }, {
      threshold: [0.1],
    });

    this.init();
  }

  #observePriceListSearch = () => {
    if (this.#media.matches) {
      this.#priceListObserver.observe(this.#priceListSearch);
    } else {
      this.#priceListObserver.unobserve(this.#priceListSearch);
      this.#element.classList.remove('header--fixed-down');
    }
  };

  #onScrolled = () => {
    if (this.#isPriceListVisible) return;

    if (document.documentElement.scrollTop > 0 && !this.#element.classList.contains('header--fixed')) {
      this.#element.classList.add('header--fixed');
    } else if (document.documentElement.scrollTop <= 0) {
      this.#element.classList.remove('header--fixed');
    }

    if (window.scrollY > this.#viewportHeight) this.#element.classList.add('header--fixed-down');
    else this.#element.classList.remove('header--fixed-down');

    if (window.scrollY < this.#lastKnownScrollPosition) this.#element.classList.remove('header--fixed-down');
    this.#lastKnownScrollPosition = window.scrollY;
  };

  #addMenuActiveClass() {
    const currentPage = window.location.pathname;
    this.#menuLinks.forEach((item) => {
      if (item.getAttribute('href') === currentPage) item.closest('.header-navigation__item').classList.add('header-navigation__item--active');
    });
  }

  init = () => {
    if (this.#priceListSearch) {
      this.#media.addEventListener('change', this.#observePriceListSearch);
      this.#observePriceListSearch();
    }
    document.addEventListener('scroll', this.#onScrolled);
    this.#switcher?.addEventListener('click', () => this.#element.classList.toggle(this.#activeClass));
    this.#addMenuActiveClass();
  };
}
