export class HomeTreatment {
  #element = document.querySelector('.home-treatment .swiper');
  #slider;
  #sliderSettings = {
    spaceBetween: 32,
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      800: {
        slidesPerView: 3,
      },
    },
  };

  constructor() {
    this.init();
  }

  init() {
    if (!this.#element) return;
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    this.#slider.init();
  }
}
