class Slider {
  #slider;
  #elem;
  #modules;
  #pagination;
  #recievedSettings;
  #paginationSettings = {
    el: '.slider__pagination',
    bulletClass: 'slider__pagination-item',
    bulletActiveClass: 'slider__pagination-item--active',
    lockClass: 'slider__pagination--lock',
  };

  #settings = {
    navigation: {
      nextEl: '.slider__arrow--next',
      prevEl: '.slider__arrow--prev',
      disabledClass: 'slider__arrow--disabled',
    },
  };

  constructor(elem, settings = {}) {
    this.#elem = elem;
    this.#pagination = elem.querySelector('.slider__pagination');
    if (this.#pagination) this.#settings.pagination = this.#paginationSettings;
    this.#recievedSettings = settings;
    Object.assign(this.#settings, this.#recievedSettings);
  }

  async init() {
    if (!this.#elem) return;

    const { Swiper } = await import('swiper');
    const { Navigation, Pagination } = await import('swiper/modules');

    this.#modules = [Navigation, Pagination];
    this.#settings.modules = this.#modules;

    this.#slider = new Swiper(this.#elem, this.#settings);
  }
}

export default Slider;
