const doctorInfo = document.querySelectorAll('.doctor-info');

export const doctorInfoInit = () => {
  doctorInfo.forEach((info) => {
    info.addEventListener('click', (evt) => {
      const { target } = evt;
      if (target.classList.contains('doctor-info__button')) {
        evt.preventDefault();
        const doctorInfoHiddenItems = info.querySelectorAll('.doctor-info__item--hidden');
        doctorInfoHiddenItems.forEach((item) => {
          item.classList.remove('doctor-info__item--hidden');
        });
        target.remove();
      }
    });
  });
};
