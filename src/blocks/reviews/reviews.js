/* eslint-disable no-param-reassign */
import { breakpoints } from '../../common/js/enums';

export const toggleReviewText = () => {
  const reviewsText = document.querySelectorAll('.reviews__body');
  const reviewsContainer = document.querySelector('.reviews');
  reviewsText.forEach((text) => {
    const fullText = text.innerHTML;
    const maxLength = 300;

    if (fullText.length > maxLength) {
      const truncatedText = `${fullText.substring(0, maxLength)}...   `;

      text.fullText = fullText;
      text.truncatedText = truncatedText;
      text.innerHTML = truncatedText;

      const toggleButton = document.createElement('a');
      toggleButton.setAttribute('href', '#');
      toggleButton.className = 'reviews__button';
      toggleButton.innerText = 'Показать еще';

      text.append(toggleButton);
    }
  });

  reviewsContainer.addEventListener('click', (event) => {
    if (event.target.classList.contains('reviews__button')) {
      event.preventDefault();
      const textElement = event.target.closest('.reviews__body');

      if (textElement.innerHTML.includes('...')) {
        textElement.innerHTML = textElement.fullText;
        textElement.append(event.target);
        event.target.innerText = 'Скрыть';
      } else {
        textElement.innerHTML = textElement.truncatedText;
        textElement.append(event.target);
        event.target.innerText = 'Показать еще';
      }
    }
  });
};

export class ReviewsSlider {
  #element = document.querySelector('.reviews .swiper');
  #slider;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 32,
    breakpoints: {
      [breakpoints.m]: {
        slidesPerView: 2,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      this.#slider.init();
    }
  };
}
