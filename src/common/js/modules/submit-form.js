import { showModal } from '../../../blocks/modal/modal';

const forms = document.querySelectorAll('.form');

const showMessage = (text) => {
  const message = document.createElement('div');
  message.className = 'modal';
  message.innerHTML = `
    <div class="modal__inner">
      <p>${text}</p>
      <button type="button" class="modal__close"></button>
    </div>
    `;
  document.body.append(message);
  showModal(message);
  message.addEventListener('modalClose', () => message.remove());
};

const sendData = async (body) => {
  try {
    const response = await fetch('https://inamed.ru/ajax/send.php', {
      method: 'POST',
      body,
    });
    return response.ok;
  } catch (error) {
    return false;
  }
};

const handleFormSubmit = (form) => async (event) => {
  event.preventDefault();
  const message = form.dataset.answerSuccess;
  const onSuccess = () => showMessage(message);
  const onFail = () => showMessage('Произошла ошибка, перезагрузите страницу');

  const formData = new FormData(event.target);

  const isSuccess = await sendData(formData);
  if (isSuccess) {
    onSuccess();
    form.reset();
  } else {
    onFail();
  }
};

const submitForm = () => {
  forms.forEach((form) => form.addEventListener('submit', handleFormSubmit(form)));
};

export default submitForm;
